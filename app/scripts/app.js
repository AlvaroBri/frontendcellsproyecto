(function(document) {
  'use strict';

  window.CellsPolymer.start({
    routes: {
      'login': '/',
      'register': '/register',
      'profile': '/profile',
      'accounts': '/accounts',
      'transactions': '/transactions',
      'operations': '/operation',
      'currency': '/currency'
    }
  });

}(document));
